const PostgrestPlugin = {
    install(Vue, options) {

        let postgrestUrl = "";
        let postgrestHeaders = {};

        if (options) {
            postgrestUrl = Object.is(options.url, undefined) ? "" : options.url;
            if (!postgrestUrl.endsWith('/'))
            {
                postgrestUrl = postgrestUrl + '/';
            }

            postgrestHeaders = Object.is(options.headers, undefined) ? {} : options.headers;
        }

        Vue.mixin({
            methods: {
                getRequest(table, query = undefined) {
                    let req = postgrestUrl + table;
                    if (query) {
                        req = req + '?' + query;
                    }

                    return req;
                },

                getHeaders() {
                    return postgrestHeaders;
                }
            }
        });
    }
};

export default PostgrestPlugin;
