CREATE TABLE category (
    id serial PRIMARY KEY ,
    shortname character varying(60) NOT NULL
);

CREATE TABLE item (
    id serial PRIMARY KEY,
    shortname character varying(200) NOT NULL,
    description character varying,
    fk_category integer references category(id),
    deleted timestamp without time zone,
    created timestamp without time zone DEFAULT now(),
    abbreviation character varying(40),
    url character varying
);
