import Vue from 'vue'
import App from '@/views/App.vue'

import CKEditor from '@ckeditor/ckeditor5-vue'
import i18n from '@/plugins/i18n'
import ToastPlugin from '@/plugins/ToastPlugin' 
import PostgrestPlugin from '@/plugins/PostgrestPlugin'
import BootstrapVue from 'bootstrap-vue'

import router from './router'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(CKEditor);
Vue.use(ToastPlugin);
Vue.use(PostgrestPlugin, {
  "url": "https://beqbznhnlexxlajyumwk.supabase.co/rest/v1",
  "headers": {
    "Content-Type": "application/json",
    "Prefer": "return=representation",
    "apikey": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImJlcWJ6bmhubGV4eGxhanl1bXdrIiwicm9sZSI6ImFub24iLCJpYXQiOjE2Nzg3NzIwMjgsImV4cCI6MTk5NDM0ODAyOH0.-rCIpAyPcv3XZpEbJtvYJXDRX4GD2jeohdlpVnWGJ5s",
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImJlcWJ6bmhubGV4eGxhanl1bXdrIiwicm9sZSI6ImFub24iLCJpYXQiOjE2Nzg3NzIwMjgsImV4cCI6MTk5NDM0ODAyOH0.-rCIpAyPcv3XZpEbJtvYJXDRX4GD2jeohdlpVnWGJ5s"
  }
});

new Vue({
  i18n,
  router,
  render: h => h(App)
}).$mount('#app')
