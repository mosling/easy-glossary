const ToastPlugin = {
    install(Vue, options) {

        var delay;
        var append;

        if (options) {
            delay = Object.is(options.delay, undefined) ? 5000 : options.delay;
            append = Object.is(options.append, undefined) ? true : options.append;
        }

        Vue.mixin({
            methods: {
                makeToast(message, response = undefined) {
                    var variant = 'info';
                    var title = this.$t("appTitle");
                    var msgExplanation = "";
                    var status = 200;
                    var msgBody = this.$t(message);

                    if (response)
                    {
                        // suggest a http status code :-)
                        title = this.$t("serverTitle");
                        status = response.status;
                        if (status >= 400)
                        {
                            variant = "danger";
                            msgExplanation = response.data.details;
                        }
                    }

                    if (msgExplanation.length > 0)
                    {
                        msgExplanation = " ( " + msgExplanation + ")";
                    }

                    this.$bvToast.toast(msgBody + "\n" + msgExplanation, {
                        title: title,
                        autoHideDelay: delay,
                        appendToast: append,
                        variant: variant
                    });
                }
            }
        });
    }
};

export default ToastPlugin;

